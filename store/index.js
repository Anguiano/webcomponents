export const state = () => ({
    sectionTitle:'',
    sectionColor:'',
    sectionIcon:[]
})

export const mutations = {
    changeSectionTitle(state, newTilte){
        state.sectionTitle = newTilte
    },
    changeSectionColor(state, newColor){
        state.sectionColor = newColor
    }, 
    changeSectionIcon(state, newIcon){
        state.sectionIcon = newIcon
    }
    
}